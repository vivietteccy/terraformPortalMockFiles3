# Configure Terraform to set the required AzureRM provider
# version and features{} block.

terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = ">= 2.77.0"
    }
  }
}

provider "azurerm" {
  features {}
}


#### test from microsoft caf

data "azurerm_client_config" "core" {}

# module "enterprise_scale" {
#   source  = "Azure/caf-enterprise-scale/azurerm"
#   version = "1.1.3"

#   providers = {
#     azurerm              = azurerm
#     azurerm.connectivity = azurerm
#     azurerm.management   = azurerm
#   }

#   root_parent_id = data.azurerm_client_config.core.tenant_id
#   root_id        = "rootId"
#   root_name      = "rootName"

# }


######################## Create Subscription
# the field name can be found in "Subscription" > "Create a New Subscription"
data "azurerm_billing_mca_account_scope" "customer_billing_scope" {
  billing_account_name = "ef0f97c2-fc62-5a95-5a3b-1667c7d12ff8:ca20607d-1451-49df-8605-88881ae1e85f_2019-05-31"
  billing_profile_name = "R6AR-4ZY5-BG7-PGB"
  invoice_section_name = "5K6H-OO4W-PJA-PGB"
}

output "id" {
  value = data.azurerm_billing_mca_account_scope.customer_billing_scope.id
}

output "tenant_id" {
  value = data.azurerm_client_config.core.tenant_id
}

resource "azurerm_subscription" "subscription_for_landing_zone" {
  subscription_name = "Landing Zone Subscription"
  billing_scope_id  = data.azurerm_billing_mca_account_scope.customer_billing_scope.id
}

resource "azurerm_subscription" "subscription_for_connectivity" {
  subscription_name = "Connectivity Subscription"
  billing_scope_id  = data.azurerm_billing_mca_account_scope.customer_billing_scope.id
}

resource "azurerm_subscription" "subscription_for_logging_and_monitoring" {
  subscription_name = "Logging and Monitoring Subscription"
  billing_scope_id  = data.azurerm_billing_mca_account_scope.customer_billing_scope.id
}


####################### Create Management Group

resource "azurerm_management_group" "management_group_landing_zone" {
  display_name               = "Landing Zone"

  subscription_ids = [
    azurerm_subscription.subscription_for_landing_zone.subscription_id,
  ]
}

resource "azurerm_management_group" "management_group_connectivity" {
  display_name               = "Connectivity"

  subscription_ids = [
    azurerm_subscription.subscription_for_connectivity.subscription_id,
  ]
}

resource "azurerm_management_group" "management_group_logging_and_monitoring" {
  display_name               = "Logging and Monitoring"

  subscription_ids = [
    azurerm_subscription.subscription_for_logging_and_monitoring.subscription_id,
  ]
}

############################# test policy assignment - built in 
resource "azurerm_subscription_policy_assignment" "policy_builtin_API_management_01" {
  name = "Built in Policy - API Management 01"
  policy_definition_id = "/providers/Microsoft.Authorization/policyDefinitions/73ef9241-5d81-4cd4-b483-8443d1730fe5"
  subscription_id = "/subscriptions/${azurerm_subscription.subscription_for_landing_zone.subscription_id}"

  # parameters = {
  #   tagName = ""
  # }
}

########################### test policy assignment - custom
locals {
  json_files = fileset("./custom_policy/", "*.json")   
  json_data  = [ for f in local.json_files : jsondecode(file("./custom_policy/${f}")) ]

  // allow dynamic mapping to subscription
  management_group_obj = {
    "lz" = azurerm_management_group.management_group_landing_zone.group_id
    "c" = azurerm_management_group.management_group_connectivity.group_id
    "lm" = azurerm_management_group.management_group_logging_and_monitoring.group_id 
  }
}

resource "azurerm_policy_definition" "custom_policy" {

  count = length(local.json_data)

  management_group_id = local.management_group_obj[local.json_data[count.index].managementGroup]

  description = local.json_data[count.index].properties.description
  display_name = local.json_data[count.index].properties.displayName

  mode = local.json_data[count.index].properties.mode
  name = local.json_data[count.index].name
  policy_type = local.json_data[count.index].properties.policyType

  metadata = jsonencode(local.json_data[count.index].properties.metadata)
  parameters = jsonencode(local.json_data[count.index].properties.parameters)
  policy_rule = jsonencode(local.json_data[count.index].properties.policyRule)

}